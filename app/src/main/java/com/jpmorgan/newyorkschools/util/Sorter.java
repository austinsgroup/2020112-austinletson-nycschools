package com.jpmorgan.newyorkschools.util;

import com.jpmorgan.newyorkschools.data.School;

import java.nio.file.Path;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Utility class to sort the school list in different ways
 */
public class Sorter {

    /**
     * Sort a schools list alphabetically
     *
     * @param schools list to sort
     * @return sorted list
     */
    public static List<School> sortByAlpha(List<School> schools) {
        if (schools == null) {
            return null;
        }
        Collections.sort(schools, new AlphaComparator());
        return schools;
    }

    private static class AlphaComparator implements Comparator<School> {
        @Override
        public int compare(School school1, School school2) {
            return school1.getName().compareTo(school2.getName());
        }
    }

    /**
     * Sort a schools list by number of students
     *
     * @param schools list to sort
     * @return sorted list
     */
    public static List<School> sortByNumStudents(List<School> schools) {
        if (schools == null) {
            return null;
        }
        Collections.sort(schools, new NumStudentComparator());
        return schools;
    }

    private static class NumStudentComparator implements Comparator<School> {
        @Override
        public int compare(School school1, School school2) {
            if (school1.getTotalStudents() > school2.getTotalStudents()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    /**
     * Sort a schools list by graduation rate
     *
     * @param schools list to sort
     * @return sorted list
     */
    public static List<School> sortByGraduationRate(List<School> schools) {
        if (schools == null) {
            return null;
        }
        Collections.sort(schools, new GraduationRateComparator());
        return schools;
    }

    private static class GraduationRateComparator implements Comparator<School> {
        @Override
        public int compare(School school1, School school2) {
            if (school1.getGraduationRate() > school2.getGraduationRate()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

}
