package com.jpmorgan.newyorkschools.ui.schools;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jpmorgan.newyorkschools.R;
import com.jpmorgan.newyorkschools.data.School;
import com.jpmorgan.newyorkschools.ui.detail.DetailActivity;

/**
 * Activity representing a list of schools which navigate to the school details activity.
 */
public class SchoolsActivity extends AppCompatActivity {

    SchoolsViewModel schoolsViewModel;

    private RecyclerView schoolRecyclerView;
    private RecyclerView.Adapter schoolsAdapter;

    public final static String EXTRA_DBN = "com.jpmorgan.newyorkschools.EXTRA_DBN";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        schoolsViewModel = new ViewModelProvider(this).get(SchoolsViewModel.class);

        // Setup the recycler view
        schoolRecyclerView = findViewById(R.id.schools_list);
        schoolRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        schoolRecyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, linearLayoutManager.getOrientation());
        schoolRecyclerView.addItemDecoration(dividerItemDecoration);

        // Observe the schools live data and when it changes set the adapter and hide the progress bar
        schoolsViewModel.getSchools().observe(this, schools -> {
            if (schools == null) {
                return;
            }
            findViewById(R.id.schools_progress_bar).setVisibility(View.GONE);
            schoolsAdapter = new SchoolsAdapter(schools, school -> {
                Intent detailsIntent = new Intent(getApplicationContext(), DetailActivity.class);
                detailsIntent.putExtra(EXTRA_DBN, school.getDbn());
                startActivity(detailsIntent);
            });
            schoolRecyclerView.setAdapter(schoolsAdapter);

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);

        // Prepare adapter for spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.schools_sort_array, R.layout.item_spinner_selected);
        adapter.setDropDownViewResource(R.layout.item_spinner_unselected);


        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Switch on the different sorting selections
                switch (position) {
                    case 0:
                        schoolsViewModel.sortSchoolsByAlpha();
                        break;
                    case 1:
                        schoolsViewModel.sortSchoolsByTotalStudents();
                        break;
                    case 2:
                        schoolsViewModel.sortSchoolsByGraduationRate();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        }); // set the listener, to perform actions based on item selection
        return true;
    }
}