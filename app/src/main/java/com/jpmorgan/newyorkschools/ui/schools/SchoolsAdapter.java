package com.jpmorgan.newyorkschools.ui.schools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jpmorgan.newyorkschools.R;
import com.jpmorgan.newyorkschools.data.School;

import java.util.List;

/**
 * Standard adapter for schools recycler view
 */
public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.ViewHolder> {
    private List<School> mSchools;
    private OnSchoolClickListener mOnSchoolClickListener;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView schoolTextView;

        public ViewHolder(View v) {
            super(v);
            schoolTextView = v.findViewById(R.id.school_item_name);
        }
    }

    /**
     * Listener for when the user presses an element in the recycler view
     */
    public interface OnSchoolClickListener {
        void onSchoolClick(School school);
    }

    public SchoolsAdapter(List<School> schools, OnSchoolClickListener onSchoolClickListener) {
        mSchools = schools;
        mOnSchoolClickListener = onSchoolClickListener;
    }

    @NonNull
    @Override
    public SchoolsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_school, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(v -> mOnSchoolClickListener.onSchoolClick(mSchools.get(position)));
        holder.schoolTextView.setText(mSchools.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mSchools.size();
    }
}
