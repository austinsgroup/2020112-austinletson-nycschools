package com.jpmorgan.newyorkschools.ui.detail;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.jpmorgan.newyorkschools.data.SatScore;
import com.jpmorgan.newyorkschools.data.School;
import com.jpmorgan.newyorkschools.data.SchoolRepository;

/**
 * View model for detail activity which keeps track of the data and talks to the repository
 */
public class DetailViewModel extends ViewModel {
    private final SchoolRepository schoolRepository = SchoolRepository.getInstance();

    private MutableLiveData<SatScore> satScore = new MutableLiveData<>();
    private MutableLiveData<School> school = new MutableLiveData<>();

    public MutableLiveData<SatScore> getSatScore(String dbn) {
        satScore = schoolRepository.getSatScore(dbn);
        return satScore;
    }

    public MutableLiveData<School> getSchool(String dbn) {
        school = schoolRepository.getSchool(dbn);
        return school;
    }


}
