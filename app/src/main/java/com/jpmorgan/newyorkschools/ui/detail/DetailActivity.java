package com.jpmorgan.newyorkschools.ui.detail;

import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProvider;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.jpmorgan.newyorkschools.R;
import com.jpmorgan.newyorkschools.ui.schools.SchoolsActivity;

/**
 * Activity to display details about a given school
 */
public class DetailActivity extends AppCompatActivity {
    DetailViewModel detailViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        detailViewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Set up the action bar with back button and no title
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.detail_action_bar_title));

        // Initialize all the text views
        // Given more time I would have used data binding to simplify this logic
        TextView textViewTitle = findViewById(R.id.detail_title);
        TextView textViewMath = findViewById(R.id.detail_math);
        TextView textViewReading = findViewById(R.id.detail_reading);
        TextView textViewWriting = findViewById(R.id.detail_writing);
        TextView textViewNumStudent = findViewById(R.id.detail_num_students);
        TextView textViewGraduationRate = findViewById(R.id.detail_graduation_rate);

        // Get dbn from extra from schools activity
        String dbn = getIntent().getStringExtra(SchoolsActivity.EXTRA_DBN);

        // Observe the sat score and set text views once it is fetched
        detailViewModel.getSatScore(dbn).observe(this, satScore -> {
            if (satScore == null) {
                textViewMath.setText(getResources().getString(R.string.detail_no_sat));
                textViewReading.setText(getResources().getString(R.string.detail_no_sat));
                textViewWriting.setText(getResources().getString(R.string.detail_no_sat));
                return;
            }
            textViewMath.setText(satScore.getMathSat());
            textViewReading.setText(satScore.getReadingSat());
            textViewWriting.setText(satScore.getWritingSat());

        });

        // Observe school and set text views once it is fetched
       detailViewModel.getSchool(dbn).observe(this, school -> {
           if (school == null) {
               textViewTitle.setText(getResources().getString(R.string.detail_title_error));
               textViewNumStudent.setText(getResources().getString(R.string.detail_no_num_students));
               textViewGraduationRate.setText(getResources().getString(R.string.detail_no_graduation_rate));
               return;
           }
           textViewTitle.setText(school.getName());
           textViewNumStudent.setText(Integer.toString(school.getTotalStudents()));
           if (school.getGraduationRate() == 0.0) {
               textViewGraduationRate.setText(getResources().getString(R.string.detail_no_graduation_rate));
           } else {
               textViewGraduationRate.setText(Double.toString(school.getGraduationRate()));
           }
       });


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Switch on menu item to finish if the user presses the back button
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}