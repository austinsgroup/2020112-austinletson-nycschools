package com.jpmorgan.newyorkschools.ui.schools;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.jpmorgan.newyorkschools.data.School;
import com.jpmorgan.newyorkschools.data.SchoolRepository;
import com.jpmorgan.newyorkschools.util.Sorter;

import java.util.List;

/**
 * View model for schools activity which keeps track of data and talks to the repository
 * The view model also sorts the data when interacts with the sorter class.
 */
public class SchoolsViewModel extends ViewModel {
    private final SchoolRepository schoolRepository = SchoolRepository.getInstance();

    private MutableLiveData<List<School>> schoolList = new MutableLiveData<>();

    public MutableLiveData<List<School>> getSchools() {
        schoolList = schoolRepository.getSchools();
        sortSchoolsByAlpha();
        return schoolList;
    }

    public void sortSchoolsByAlpha() {
        schoolList.setValue(Sorter.sortByAlpha(schoolList.getValue()));
    }

    public void sortSchoolsByTotalStudents() {
        schoolList.setValue(Sorter.sortByNumStudents(schoolList.getValue()));
    }

    public void sortSchoolsByGraduationRate() {
        schoolList.setValue(Sorter.sortByGraduationRate(schoolList.getValue()));
    }

}
