package com.jpmorgan.newyorkschools.data;

import androidx.lifecycle.MutableLiveData;

import com.jpmorgan.newyorkschools.util.Sorter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Repository for school and SAT databases. Contains methods to retrieve lists of schools, individual schools, and SAT scores.
 */
public class SchoolRepository {

    private static SchoolRepository schoolRepository;

    public static SchoolRepository getInstance() {
        if (schoolRepository == null) {
            schoolRepository = new SchoolRepository();
        }
        return schoolRepository;
    }

    private SchoolApi schoolApi;

    private SchoolRepository() {
        schoolApi = RetrofitService.createService(SchoolApi.class);
    }

    /**
     * Method to get a list of schools from the database
     *
     * @return a list of schools. If the request fails this is null,
     */
    public MutableLiveData<List<School>> getSchools() {
        MutableLiveData<List<School>> schoolList = new MutableLiveData<>();
        schoolApi.schools().enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                if (response.isSuccessful()) {
                    schoolList.postValue(Sorter.sortByAlpha(response.body()));
                }
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                schoolList.postValue(null);

            }
        });
        return schoolList;
    }

    /**
     * Method to get a single school from the server. This may seem redundant and wasteful compared to the getSchools()
     * method, however in a real application the schools would be cached on the device and this abstraction creates
     * a more elegant interface for the view models to access individual schools as opposed to passing information between
     * activities.
     *
     * @param dbn the database number for the school
     * @return live data for school object. If the requet fails then this returns null.
     */
    public MutableLiveData<School> getSchool(String dbn) {
        MutableLiveData<School> school = new MutableLiveData<>();
        schoolApi.school(dbn).enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                // Response should never be empty but just in case
                if (response.body().size() == 0) {
                    school.postValue(null);
                    return;
                }
                school.postValue(response.body().get(0));
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                school.postValue(null);
            }
        });
        return school;
    }

    /**
     * Method to get SAT score from SAT database given a database number.
     *
     * @param dbn the database number for the school
     * @return live data for the SAT score. If there is no SAT data for the given school or if the request
     * fails, then this returns null
     */
    public MutableLiveData<SatScore> getSatScore(String dbn) {
        MutableLiveData<SatScore> satScore = new MutableLiveData<>();
        schoolApi.satScore(dbn).enqueue(new Callback<List<SatScore>>() {
            @Override
            public void onResponse(Call<List<SatScore>> call, Response<List<SatScore>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size() == 0) {
                        satScore.postValue(null);
                        return;
                    }
                    satScore.postValue(response.body().get(0));
                }
            }

            @Override
            public void onFailure(Call<List<SatScore>> call, Throwable t) {
                satScore.postValue(null);
            }
        });
        return satScore;
    }

}
