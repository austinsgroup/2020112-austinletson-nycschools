package com.jpmorgan.newyorkschools.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model class for SAT score. Works with JSON
 */
public class SatScore {

    @SerializedName("dbn")
    @Expose
    private String dbn;

    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    private String readingSat;

    @SerializedName("sat_math_avg_score")
    @Expose
    private String mathSat;

    @SerializedName("sat_writing_avg_score")
    @Expose
    private String writingSat;

    public String getDbn() {
        return dbn;
    }

    public String getReadingSat() {
        return readingSat;
    }

    public String getMathSat() {
        return mathSat;
    }

    public String getWritingSat() {
        return writingSat;
    }
}