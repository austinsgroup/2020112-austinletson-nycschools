package com.jpmorgan.newyorkschools.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model class for school. Works with JSON
 */
public class School {

    @SerializedName("dbn")
    @Expose
    private String dbn;

    @SerializedName("school_name")
    @Expose
    private String name;

    @SerializedName("total_students")
    @Expose
    private int totalStudents;

    @SerializedName("graduation_rate")
    @Expose
    private double graduationRate;

    public School(String dbn, String name, int totalStudents, double graduationRate) {
        this.dbn = dbn;
        this.name = name;
        this.totalStudents = totalStudents;
        this.graduationRate = graduationRate;
    }

    public String getDbn() {
        return dbn;
    }

    public String getName() {
        return name;
    }

    public int getTotalStudents() {
        return totalStudents;
    }

    public double getGraduationRate() {
        return graduationRate;
    }
}
