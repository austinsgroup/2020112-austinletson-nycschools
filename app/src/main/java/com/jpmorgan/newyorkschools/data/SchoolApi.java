package com.jpmorgan.newyorkschools.data;

import android.app.DownloadManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * API for accessing schools and SAT scores
 */
public interface SchoolApi {

    @GET("/resource/s3k6-pzi2.json")
    Call<List<School>> schools();

    @GET("/resource/f9bf-2cp4.json")
    Call<List<SatScore>> satScore(@Query("dbn") String dbn);

    @GET("/resource/s3k6-pzi2.json")
    Call<List<School>> school(@Query("dbn") String dbn);
}
