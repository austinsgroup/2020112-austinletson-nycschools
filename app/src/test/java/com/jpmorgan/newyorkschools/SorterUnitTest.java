package com.jpmorgan.newyorkschools;

import com.jpmorgan.newyorkschools.data.School;
import com.jpmorgan.newyorkschools.util.Sorter;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit test to verify that the sorter utility functions properly
 */
public class SorterUnitTest {
    List<School> schools1 = new ArrayList<>();
    List<School> schools2 = new ArrayList<>();
    List<School> schoolsSorted = new ArrayList<>();

    @Before
    public void prepare() {

        School school1 = new School("1", "a", 10, .1);
        School school2 = new School("2", "b", 20, .2);
        School school3 = new School("3", "c", 30, .3);
        School school4 = new School("4", "d", 40, .4);

        schools1.add(school2);
        schools1.add(school1);
        schools1.add(school4);
        schools1.add(school3);

        schools2.add(school4);
        schools2.add(school3);
        schools2.add(school2);
        schools2.add(school1);

        schoolsSorted.add(school1);
        schoolsSorted.add(school2);
        schoolsSorted.add(school3);
        schoolsSorted.add(school4);

    }

    @Test
    public void alphaSort_isCorrect() {
        assertEquals(Sorter.sortByAlpha(schools1), schoolsSorted);
        assertEquals(Sorter.sortByGraduationRate(schools2), schoolsSorted);
    }

    @Test
    public void numStudentsSort_isCorrect() {
        assertEquals(Sorter.sortByNumStudents(schools1), schoolsSorted);
        assertEquals(Sorter.sortByGraduationRate(schools2), schoolsSorted);
    }

    @Test
    public void graduationRateSort_isCorrect() {
        assertEquals(Sorter.sortByGraduationRate(schools1), schoolsSorted);
        assertEquals(Sorter.sortByGraduationRate(schools2), schoolsSorted);
    }
}